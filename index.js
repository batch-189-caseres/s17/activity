/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function userDetails() {

        // Prompt User Details
        let userName = prompt('What is your full name?');
        let userAge = prompt('What is your age?');
        let userLocation = prompt('What is your location?');

        // Print the user input into the console
        console.log('Hello, ' + userName + '.');
        console.log('You are ' + userAge + ' years old.');
        console.log('You live in ' + userLocation + '.');

    }

    userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function myFavoriteBands() {
        console.log('1. Parokya ni Edgar\n2. Rivermaya\n3. The Vamps\n4. Paramore\n5. One Direction');
    };

    myFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function myFavoriteMovies() {
        console.log('1. SPIDER-MAN: NO WAY HOME (2021)\nRotten Tomatoes Rating: 93%');
        console.log('2. Cars (2006)\nRotten Tomatoes Rating: 74%');
        console.log('3. SPIDER-MAN: INTO THE SPIDER-VERSE (2018)\nRotten Tomatoes Rating: 97%');
        console.log('4. Avengers: Endgame (2019)\nRotten Tomatoes Rating: 94%');
        console.log('5. Doctor Strange in the Multiverse of Madness (2022)\nRotten Tomatoes Rating: 74%');
    }

    myFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
    // printUsers();
    let printFriends = function /*printUsers*/(){
        alert("Hi! Please add the names of your friends.");
        let friend1 = prompt("Enter your first friend's name:"); 
        let friend2 = prompt("Enter your second friend's name:"); 
        let friend3 = prompt("Enter your third friend's name:");

        console.log("You are friends with:")
        console.log(friend1); 
        console.log(friend2); 
        console.log(friend3); 
    };

    printFriends();
    
    // console.log(friend1);
    // console.log(friend2);
